import minio
import os
import uuid

def get_abspath(dir_path,list_name):
   
    
   
    for file in os.listdir(dir_path):  # 获取文件（夹）名
        file_path = os.path.join(dir_path, file)  # 将文件（夹）名补全为路径
        #file_path = file
        if os.path.isdir(file_path):  # 如果是文件夹，则递归
            get_abspath(file_path,list_name)
        else:
            list_name.append(file_path)  # 保存路径
    return list_name

def get_relpath(list_name,abspath):
    result = []
    for sub in list_name:
        result.append(sub.replace(abspath,""))
    return result

class minioClient:
    def __init__(self,endpoint,access_key,secret_key):
        self.endpoint=endpoint
        self.access_key=access_key 
        self.secret_key=secret_key

        self.minio_conf = {
            'endpoint': self.endpoint,
            'access_key': self.access_key,
            'secret_key': self.secret_key,
            'secure': False
        }

        self.client = minio.Minio(**self.minio_conf)


    def uploadModel(self,localPath,bucketName,remoteFolder="default"):
        try:
            if(self.client.bucket_exists(bucketName)):
                pass
            else:
                self.client.make_bucket(bucketName)

            if os.path.isfile(localPath):
                absPath = os.path.dirname(localPath)+"/"
                relPath = localPath.replace(absPath,"")
                self.client.fput_object(bucket_name=bucketName, object_name=remoteFolder+"/"+relPath,
                            file_path=localPath,
                            content_type='application/zip'
                            ) 
                return True, bucketName, remoteFolder+"/"+relPath
            else:

                absPath = os.path.dirname(localPath)+"/"
                fileName = localPath.replace(absPath,"")
                print("fileName "+str(fileName))
                reclusivePathList = get_abspath(localPath,[])
                print("reclusivePathList "+str(reclusivePathList))
                relPathList = get_relpath(reclusivePathList,absPath)
                print("relPathList "+str(relPathList))
            
                for i in range(0, len(reclusivePathList)):
                    self.client.fput_object(bucket_name=bucketName, object_name=remoteFolder+"/"+relPathList[i],
                                file_path=reclusivePathList[i],
                                content_type='application/zip'
                                )
                return True, bucketName, remoteFolder+"/"+fileName
        except Exception as e:
            print(e)
            return False, bucketName, None

    def uploadDataset(self,localPath,bucketName,remoteFolder="default"):

        #传入 本地数据集文件夹， 远端桶，远端主路径（默认是default）
        # 返回 bool, 远端桶，远端zip包路径
        absPath = os.path.dirname(localPath)+"/"
        fileName = localPath.replace(absPath,"")
        
        
        print("cd "+absPath+" && zip -r "+fileName+".zip "+fileName)
        os.system("cd "+absPath+" && zip -r "+fileName+".zip "+fileName)

        zipLocalPath = fileName+".zip"

        print("zipLocalPath "+ zipLocalPath)
        print(localPath+".zip")
        try:
            if(self.client.bucket_exists(bucketName)):
                pass
            else:
                self.client.make_bucket(bucketName)

            if os.path.isfile(localPath+".zip"):
                self.client.fput_object(bucket_name=bucketName, object_name=remoteFolder+"/"+zipLocalPath,
                            file_path=localPath+".zip",
                            content_type='application/zip'
                            )

                os.system("rm -rf "+localPath+".zip")
                return True, bucketName, remoteFolder+"/"+zipLocalPath
            else:
                return False, bucketName, None
            
        except Exception as e:
            print(e)
            return False, bucketName, None




    def downloadDataset(self,localPath,bucketName,remoteFolder):
        #目前先支持文件下载
        # 传入 本地文件夹的绝对路径(用于存储解压后的文件) 云端桶名称，云端zip包路径
        #返回 bool， 文件下载并解压完的路径
        os.system("mkdir -p "+localPath)
        if not self.client.bucket_exists(bucketName):
            return False, None
        data = self.client.get_object(bucketName, remoteFolder)

        tempZipName = str(uuid.uuid1())+".zip"
        tempZipPath = localPath+"/"+tempZipName
        
        with open(tempZipPath, 'wb') as file_data:
            for d in data.stream(32 * 1024):
                file_data.write(d)
        os.system("cd "+localPath+ "&& unzip "+tempZipPath + " -d "+tempZipPath[:-4])
        os.system("rm -rf "+tempZipPath)
        #os.system("cd "+localPath+ "&& unzip "+tempZipPath+ " "+ localPath)
        
        return True, tempZipPath[:-4]+"/"+os.listdir(tempZipPath[:-4])[0]

#client = minioClient("47.96.106.97:9000","minio","minio123")
#入参 第一个参数是 minio的ip ，第二个参数是用户名，第三个参数是密码 
#返回minioClient

#client.uploadData("/home/tmind/temp/1206/minios3/mnist-torch","bucket3","remoteFile2")
#入参 第一个参数是 绝对路径的本地文件夹 或 绝对路径的本地文件，第二个参数是存储桶名字，第三个参数是云端路径
#返回参数 是否成功， 云端路径

#c,d = client.downloadData("/home/tmind/temp/1206/minios3","bucket3","remoteFile2/mnist-torch/config.zip")
#入参 第一个参数是 绝对路径的本地文件夹，第二个参数是存储桶名字，第三个参数是云端路径文件（非文件夹）
#返参 第一个参数是否成功，第二个参数，下载到的本地路径


#上传数据集，先打包再上传s3
#下载数据集，从s3下载zip,然后解压
#上传模型，上传整个文件
#部署模型
