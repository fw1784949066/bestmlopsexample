# /etc/docker/daemon.json "insecure-registries": ["114.55.64.131:30012"],
# docker login 114.55.64.131:30012 admin 4nhtkkW7Vh7Ek7dNzsTW
docker build -t jupyter_deploy_seldon_torch .
docker tag jupyter_deploy_seldon_torch 114.55.64.131:30012/deploy_seldon/jupyter_deploy_seldon_torch:latest
docker image push 114.55.64.131:30012/deploy_seldon/jupyter_deploy_seldon_torch:latest